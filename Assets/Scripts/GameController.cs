﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace OnlineA.TextAdventure
{
    [RequireComponent(typeof(RoomNavigation), typeof(TextInput), typeof(ItemsManager))]
    class GameController : MonoBehaviour
    {
        /// <summary> A manager handling moving between rooms. </summary>
        public RoomNavigation RoomNavigation { get; private set; }
        /// <summary> A manager handling item manipulation. </summary>
        public ItemsManager ItemsManager { get; private set; }

        /// <summary> A log containing all the text displayed onscreen since the beginning of the game. </summary>
        List<string> actionLog = new List<string>();

        /// <summary> Displays the possible interactions in the current room. </summary>
        public List<string> RoomInteractions { get; } = new List<string>();

        /// <summary> The <see cref="actionLog"/> connected with newlines. </summary>
        string FullActionLog => string.Join("\n", actionLog.ToArray());

        /// <summary> The last few lines of the <see cref="actionLog"/> connected with newlines. </summary>
        string PartialActionLog(int lines) => string.Join("\n",
                                               actionLog.Skip(Mathf.Max(0, actionLog.Count - lines)));

        [SerializeField, Tooltip("The text field for displaying the main game text.")]
        TMP_Text displayText = default;
        [SerializeField, Tooltip("All possible actions the player can use.")]
        InputAction[] inputActions = default;
        public InputAction[] InputActions => inputActions;

        void Awake()
        {
            RoomNavigation = GetComponent<RoomNavigation>();
            ItemsManager = GetComponent<ItemsManager>();
        }

        void Start()
        {
            LogNewRoomText();
            DisplayLoggedText();
        }

        public void DisplayLoggedText()
        {
            displayText.text = FullActionLog;
            displayText.ForceMeshUpdate();
            if (displayText.isTextOverflowing && actionLog.Count > 2)
            {
                Canvas.ForceUpdateCanvases();
                float textAreaHeight = displayText.GetComponent<RectTransform>().rect.height;
                float lineHeight = displayText.textInfo.lineInfo[0].lineHeight;
                float maxLines = Mathf.FloorToInt(textAreaHeight / lineHeight) - 1;
                int toDisplay = 0;
                foreach (string log in actionLog.AsEnumerable().Reverse())
                {
                    int lines = log.Count(c => c == '\n') + 1;
                    if (maxLines - lines >= 0)
                    {
                        maxLines -= lines;
                        toDisplay++;
                    }
                }
                displayText.text = PartialActionLog(toDisplay);
            }
        }

        public void LogNewRoomText()
        {
            UnpackNewRoom();
            string interactions = string.Join("\n", RoomInteractions.ToArray());
            string combinedAll = RoomNavigation.CurrentRoom.Description + "\n" + interactions;
            LogAction(combinedAll);
        }

        /// <summary> Prepare the newly accessed room. </summary>
        void UnpackNewRoom()
        {
            CleanUpOldRoom();
            RoomNavigation.UnpackExits();
            PrepareObjects(RoomNavigation.CurrentRoom);
        }

        /// <summary> Clean all data related to the previous room. </summary>
        void CleanUpOldRoom()
        {
            ItemsManager.CleanCollections();
            RoomInteractions.Clear();
            RoomNavigation.ClearExists();
        }

        /// <summary> Prepare the objects in a room to be taken or examined. </summary>
        void PrepareObjects(Room currentRoom)
        {
            for (int i = 0; i < currentRoom.Objects.Length; i++)
            {
                string itemDescription = ItemsManager.GetRoomObjectDescription(currentRoom, i);
                if (!string.IsNullOrWhiteSpace(itemDescription))
                {
                    // Not in the inventory
                    RoomInteractions.Add(itemDescription);
                }
                InteractableObject interactableObject = currentRoom.Objects[i];
                foreach (Interaction interaction in interactableObject.Interactions)
                {
                    Dictionary<string, string> interactionDictionary = null;
                    switch (interaction.inputAction)
                    {
                        case Examine _:
                            interactionDictionary = ItemsManager.examineDictionary;
                            break;
                        case Take _:
                            interactionDictionary = ItemsManager.takeDictionary;
                            break;
                    }
                    interactionDictionary?.Add(interactableObject.Name, interaction.textResponse);
                }
            }
        }

        /// <summary> Checks that a specified interaction is possible with a given item. </summary>
        public string ValidateInteraction(Dictionary<string, string> verbDictionary, string verb, string noun)
        {
            return verbDictionary.ContainsKey(noun)
                    ? verbDictionary[noun]
                    : $"You can't <b>{verb}</b> {noun}.";
        }

        /// <summary> Logs a performed action to the action log with a newline. </summary>
        public void LogAction(string action)
        {
            actionLog.Add(action + "\n");
        }
    }
}
