﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using UnityEngine;

namespace OnlineA.TextAdventure
{
    static class TextUtils
    {
        /// <summary> The default color to use to highlight keywords with. </summary>
        internal static readonly Color32 ColorTint = new Color32(0xF7, 0x95, 0x45, 0xFF);

        /// <summary> The default color to use to highlight keywords with. </summary>
        internal static readonly Color32 PdfTint = new Color32(0xFF, 0x73, 0x00, 0xFF);

        /// <summary> Changes the colour of a word in a text. </summary>
        /// <param name="raw"> The plain <see langword="string"/> input. </param>
        /// <param name="word"> The word to highlight. </param>
        /// <param name="color"> The colour to change the text to. </param>
        /// <param name="caseSensitive"> Highlight also </param>
        [Pure] internal static string Highlight(this string raw, string word, Color color, bool caseSensitive = false)
        {
            string hex = ColorUtility.ToHtmlStringRGB(color);
            string GetColorWord(string w) => $"<color=#{hex}>{w}</color>";
            string result = caseSensitive 
                    ? raw.Replace(word, GetColorWord(word)) 
                    : Regex.Replace(raw, $"({word})", GetColorWord("$1"), RegexOptions.IgnoreCase);
            return result;
        }

        /// <summary> Compares a string collection while ignoring the case of the key. </summary>
        internal static bool ContainsIgnoreCase(this IEnumerable<string> strings, string key)
        {
            return strings.Any(item => item.Equals(key, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary> Removes rich text tags from a string. </summary>
        internal static string StripRichText(this string str)
        {
            return Regex.Replace(str, "<.*?>", string.Empty).Trim();
        }
    }
}
