﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using TMPro;

namespace OnlineA.TextAdventure
{
    public class TextInput : MonoBehaviour
    {
        /// <summary> The controller handling most of the gameplay and player actions. </summary>
        GameController controller;
        /// <summary> The input field for entering player commands. </summary>
        [SerializeField, Tooltip("The input field for entering player commands.")]
        TMP_InputField inputField = default;

        [SerializeField, Tooltip("Which colour to use to highlight keywords.")]
        Color actionTint = TextUtils.PdfTint;

        [SerializeField] bool focusOnStart = true;

        void Awake()
        {
            controller = GetComponent<GameController>();
            inputField.onValueChanged.AddListener(OnValueChanged);
            inputField.onEndEdit.AddListener(input => OnEndEdit(input));
        }

        IEnumerator Start()
        {
            if (focusOnStart)
            {
                yield return new WaitForEndOfFrame();
                inputField.ActivateInputField();
                inputField.Select();
            }
        }

        void OnValueChanged(string input)
        {
            // Highlight keywords
            string[] splits = input.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            string verb = splits.FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(verb))
            {
                if (!verb.StartsWith("<color="))
                {
                    foreach (InputAction action in controller.InputActions)
                    {
                        if (action.Keyword.Equals(verb, StringComparison.InvariantCultureIgnoreCase))
                        {
                            inputField.text = input.Highlight(verb, actionTint);
                            inputField.caretPosition = inputField.text.Length;
                        }
                    }
                }
                else if (splits.Length == 1 && splits[0].StripRichText().Length == 0)
                {
                    //! Empty input field, but with rich text tag in the beggining
                    inputField.text = string.Empty;
                }
            }
        }

        /// <summary> Split raw input into keywords for further use. </summary>
        /// <param name="input">Raw player input.</param>
        /// <param name="onLostFocus">Process input also when the input field loses focus, not only on submit.</param>
        void OnEndEdit(string input, bool onLostFocus = false)
        {
            input = input.ToLowerInvariant().Trim();
            if (!string.IsNullOrWhiteSpace(input) && (onLostFocus || Input.GetButtonDown("Submit")))
            {
                controller.LogAction(input);
                char[] delimiters = {' '};
                string[] separatedWords = input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                string command = separatedWords[0].StripRichText();
                bool responded = false;
                foreach (InputAction action in controller.InputActions)
                {
                    if (action.Keyword.Equals(command, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Found matching input action
                        responded = true;
                        action.RespondToInput(controller, separatedWords);
                    }
                }
                if (!responded)
                {
                    controller.LogAction($"{command} is an invalid action.");
                }
                CompleteInput();
            }
        }

        /// <summary> React to input after it has been processed. </summary>
        void CompleteInput()
        {
            controller.DisplayLoggedText();
            inputField.ActivateInputField();
            inputField.text = string.Empty;
        }
    }
}
