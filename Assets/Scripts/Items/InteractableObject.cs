﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Interactable Object")]
    public class InteractableObject : ScriptableObject
    {
        [SerializeField, Tooltip("The name of the interactable object.")] new string name = default;
        /// <summary> The name of the interactable object. </summary>
        public string Name => name;

        [SerializeField, Tooltip("The colour tint to hightlight the item name with.")] Color itemNameTint = TextUtils.PdfTint;

        [SerializeField, TextArea, Tooltip("The description of the interactable object as found in a room.")]
        string descriptionInRoom = default;
        /// <summary> The description of the interactable object. </summary>
        public string Description => descriptionInRoom.Highlight(Name, itemNameTint);

        [SerializeField, Tooltip("Player interactions possible with the object.")]
        Interaction[] interactions = default;

        /// <summary> Player interactions possible with the object. </summary>
        public Interaction[] Interactions => interactions;
    }
}
