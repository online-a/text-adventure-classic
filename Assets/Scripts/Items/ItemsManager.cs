﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace OnlineA.TextAdventure
{
    /// <summary> A manager handling interactable objects. </summary>
    class ItemsManager : MonoBehaviour
    {
        [SerializeField] List<InteractableObject> usableObjects = new List<InteractableObject>();

        /// <summary> The names of interactable items found in the current room. </summary>
        public List<string> ItemsInRoom { get; } = new List<string>();
        /// <summary> The names of interactable item found in the player's inventory. </summary>
        public List<string> ItemsInInventory { get; } = new List<string>();
        /// <summary> A collection mapping <see cref="Examine"/> descriptions to interactable objects. </summary>
        public Dictionary<string, string> examineDictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary> A collection mapping <see cref="Take"/> descriptions to interactable objects. </summary>
        public Dictionary<string, string> takeDictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        /// <summary> The controller handling most of the gameplay and player actions. </summary>
        GameController controller;

        Dictionary<string, ActionResponse> useDictionary = new Dictionary<string, ActionResponse>(StringComparer.InvariantCultureIgnoreCase);


        void Awake()
        {
            controller = GetComponent<GameController>();
        }

        /// <summary>
        ///     Returns the description of an interactable object if it is present in the specified room,
        ///     and isn't already in the player's inventory.
        /// </summary>
        /// <param name="currentRoom">The room to look in.</param>
        /// <param name="i">The index of the desired object</param>
        public string GetRoomObjectDescription(Room currentRoom, int i)
        {
            InteractableObject interactable = currentRoom.Objects[i];                                                       // TODO: Move currentRoom up
            if (!ItemsInInventory.Any(item => item.Equals(interactable.name, StringComparison.InvariantCultureIgnoreCase))) // case insensitive "Contains"
            {
                ItemsInRoom.Add(interactable.Name);
                return interactable.Description;
            }
            return string.Empty; // already in the inventory
        }

        /// <summary> Adds uses for every item in the inventory. </summary>
        public void AddUses()
        {
            foreach (string itemName in ItemsInInventory)
            {
                InteractableObject usable = GetUsableObject(itemName);
                if (usable == null) continue;
                foreach (Interaction interaction in usable.Interactions)
                {
                    if (interaction.response == null) continue;
                    if (!useDictionary.ContainsKey(itemName)) // not already there
                    {
                        useDictionary.Add(itemName, interaction.response);
                    }
                }
            }
        }

        /// <summary> Retrieves an usable object from the <see cref="usableObjects"/> collection. </summary>
        /// <param name="itemName"> The <see cref="InteractableObject.Name"/> to look for.</param>
        InteractableObject GetUsableObject(string itemName)
        {
            return usableObjects.FirstOrDefault(o => o.Name.Equals(itemName, StringComparison.InvariantCultureIgnoreCase));
        }

        public void DisplayInventory()
        {
            controller.LogAction("You look in your backpack, inside you have: ");
            foreach (string itemName in ItemsInInventory)
            {
                controller.LogAction(itemName); // TODO: Colour Tint
            }
        }

        /// <summary> Cleans all collections related to the items in the current room. </summary>
        public void CleanCollections()
        {
            examineDictionary.Clear();
            takeDictionary.Clear(); // TODO: Passing items between rooms?
            ItemsInRoom.Clear();
        }

        public Dictionary<string, string> Take(string[] inputWords)
        {
            if (inputWords.Length > 1)
            {
                string itemName = inputWords[1];
                if (ItemsInRoom.ContainsIgnoreCase(itemName)) // case insensitive "Contains"
                {
                    ItemsInInventory.Add(itemName);
                    AddUses();
                    ItemsInRoom.Remove(itemName);
                    return takeDictionary;
                }
                else
                {
                    controller.LogAction($"There is no {itemName} here to take.");
                    return null;
                }
            }
            controller.LogAction("You didn't specify an item to take.");
            return null;
        }

        public void Use(string[] inputWords)
        {
            if (inputWords.Length > 1)
            {
                string itemName = inputWords[1];
                if (ItemsInInventory.ContainsIgnoreCase(itemName))
                {
                    if (useDictionary.Keys.ContainsIgnoreCase(itemName))
                    {
                        // Use the item
                        bool actionResult = useDictionary[itemName].PerformActionResponse(controller);
                        // TODO: Remove item & make action permanent
                        if (!actionResult)
                        {
                            controller.LogAction("Nothing interesting happens."); // failed use action
                        }
                    }
                    else
                    {
                        // Item exists, cannot be used
                        controller.LogAction($"You can't use the {itemName}.");
                    }
                }
                else
                {
                    // No such item exists
                    controller.LogAction($"There is no {itemName} in your inventory to use.");
                }
            }
            // no word entered
        }
    }
}
