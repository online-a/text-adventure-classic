﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    /// <summary> An interaction possible with an <see cref="InteractableObject"/>. </summary>
    [System.Serializable]
    public struct Interaction
    {
        /// <summary> The input action to trigger this interaction. </summary>
        [Tooltip("The input action to trigger this interaction.")]
        public InputAction inputAction;

        /// <summary> The textual response in reaction to selecting this action. </summary>
        [TextArea, Tooltip("The textual response in reaction to selecting this action.")]
        public string textResponse;

        /// <summary> The response text shown after performing an action. </summary>
        [Tooltip("The response text shown after performing an action.")]
        public ActionResponse response;
    }
}
