﻿using System.Collections.Generic;
using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Input Actions/Inventory")]
    class Inventory : InputAction
    {
        internal override void RespondToInput(GameController controller, params string[] inputWords)
        {
            controller.ItemsManager.DisplayInventory();
        }
    }
}
