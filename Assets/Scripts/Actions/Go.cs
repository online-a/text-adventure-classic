﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Input Actions/Go")]
    class Go : InputAction
    {
        internal override void RespondToInput(GameController controller, params string[] inputWords)
        {
            controller.RoomNavigation.TryChangeRoom(direction: inputWords[1]); // TODO : Check length
        }
    }
}
