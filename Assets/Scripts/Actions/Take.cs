﻿using System.Collections.Generic;
using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Input Actions/Take")]
    class Take : InputAction
    {
        internal override void RespondToInput(GameController controller, params string[] inputWords)
        {
            Dictionary<string, string> takeDictionary = controller.ItemsManager.Take(inputWords);
            if (takeDictionary != null)
            {
                // Successfully taken
                string action = controller.ValidateInteraction(takeDictionary, inputWords[0], inputWords[1]);
                controller.LogAction(action);
            }
        }
    }
}
