﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Input Actions/Examine")]
    class Examine : InputAction
    {
        internal override void RespondToInput(GameController controller, params string[] inputWords)
        {
            string action = controller.ValidateInteraction(controller.ItemsManager.examineDictionary, 
                                                           inputWords[0], inputWords[1]);
            controller.LogAction(action);
        }
    }
}
