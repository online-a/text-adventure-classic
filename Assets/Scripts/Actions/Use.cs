﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Input Actions/Use")]
    class Use : InputAction
    {
        internal override void RespondToInput(GameController controller, params string[] inputWords)
        {
            controller.ItemsManager.Use(inputWords);
        }
    }
}
