﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    public abstract class ActionResponse : ScriptableObject
    {
        [SerializeField] string requiredString = default;
        public string RequiredString => requiredString;

        internal abstract bool PerformActionResponse(GameController controller);
    }
}
