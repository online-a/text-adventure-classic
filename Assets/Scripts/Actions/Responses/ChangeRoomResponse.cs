﻿using System;
using UnityEngine;

namespace OnlineA.TextAdventure
{
    [CreateAssetMenu(menuName = Constants.Menu + "Action Response/Change Room")]
    class ChangeRoomResponse : ActionResponse
    {
        [SerializeField, Tooltip("The room to change to.")] Room nextRoom = default;
        /// <summary> The room to change to. </summary>
        public Room NextRoom => nextRoom;

        internal override bool PerformActionResponse(GameController controller)
        {
            if (controller.RoomNavigation.CurrentRoom.RoomName.Equals(RequiredString, StringComparison.InvariantCultureIgnoreCase))
            {
                controller.RoomNavigation.CurrentRoom = NextRoom; // TODO: Change room via RoomNavigation
                controller.LogNewRoomText();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
