﻿using System;
using UnityEngine;

namespace OnlineA.TextAdventure
{

    /// <summary> A possible action the player can do. </summary>
    public abstract class InputAction : ScriptableObject
    {
        // [SerializeField] string keyword = default;
        /// <summary> The keyword used to trigger this input action. </summary>
        public string Keyword => GetType().Name.ToLowerInvariant();// keyword.ToLowerInvariant();
        /// <summary> React to a set of input keywords. </summary>
        internal abstract void RespondToInput(GameController controller, params string[] inputWords);

        /// <summary> Compares a given keyword with a specified <see cref="InputAction"/> type. </summary>
        /// <typeparam name="T"><see cref="InputAction"/> to compare.</typeparam>
        /// <param name="keyword">A command specified by the user.</param>
        public static bool IsKeyword<T>(string keyword) where T : InputAction
        {
            return string.Equals(typeof(T).Name, keyword.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}