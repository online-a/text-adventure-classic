﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnlineA.TextAdventure
{
    /// <summary> A class handling navigation between different rooms. </summary>
    class RoomNavigation : MonoBehaviour
    {
        /// <summary> The room in which the player is currently in. </summary>
        [SerializeField, Tooltip("The room in which the player is currently in.")]
        Room currentRoom = default;

        /// <summary> The room in which the player is currently in. </summary>
        public Room CurrentRoom
        {
            get { return currentRoom; }
            set { currentRoom = value; }
        }

        [SerializeField, Tooltip("The colour tint of keywords.")] Color directionTint = TextUtils.PdfTint;
        /// <summary> The controller handling most of the gameplay and player actions. </summary>
        GameController controller;

        /// <summary> A collection of exits accessible from the current room via a directional keyword. </summary>
        Dictionary<string, Room> exitMap = new Dictionary<string, Room>(StringComparer.InvariantCultureIgnoreCase);

        void Awake()
        {
            controller = GetComponent<GameController>();
        }

        /// <summary> Load all exists as possible actions. </summary>
        public void UnpackExits()
        {
            foreach (Exit exit in currentRoom.Exits)
            {
                if (string.IsNullOrWhiteSpace(exit.keyString) || !exit.connectedRoom) continue; // empty exit or no access point

                exitMap.Add(exit.keyString, exit.connectedRoom);
                string highlightedExitDescription = exit.exitDescription.Length > 0
                        ? exit.exitDescription.Highlight(exit.keyString, directionTint)
                        : string.Empty;
                controller.RoomInteractions.Add(highlightedExitDescription);
            }
        }

        /// <summary> Clean up the possible exits. </summary>
        public void ClearExists()
        {
            exitMap.Clear();
        }

        /// <summary> Go in the specified direction if it's possible. </summary>
        /// <param name="direction"> The directional keyword to access a room from this room. </param>
        /// <returns> True if the room was correctly changed. </returns>
        public bool TryChangeRoom(string direction)
        {
            if (exitMap.ContainsKey(direction))
            {
                currentRoom = exitMap[direction];
                controller.LogAction("You head off to the " + direction + "."); // TODO: Not like this
                controller.LogNewRoomText(); // TODO: Separate from direction check
                return true;
            }
            else
            {
                controller.LogAction("There is no path to the " + direction + "."); // TODO: Not like this
                return false;
            }
        }
    }
}
