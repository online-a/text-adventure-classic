﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace OnlineA.TextAdventure
{
    /// <summary> An exit from a <see cref="Room"/>, connecting it to another <see cref="Room"/>. </summary>
    [Serializable]
    public struct Exit
    {
        /// <summary> The room accessible through this <see cref="Exit"/>. </summary>
        [FormerlySerializedAs("linkedRoom"), Tooltip("The room accessible through this exit.")]
        public Room connectedRoom;
        /// <summary> The directional keyword to access this exit. </summary>
        [Tooltip("The directional keyword to access this exit.")]
        public string keyString;
        /// <summary> A flavour text describing this exit to the player. Should contain the directional keyword. </summary>
        [TextArea, Tooltip("A flavour text describing this exit to the player. Should contain the directional keyword.")]
        public string exitDescription;
    }
}
