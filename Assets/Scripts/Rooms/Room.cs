﻿using UnityEngine;

namespace OnlineA.TextAdventure
{
    /// <summary> A single room within the text adventure game. </summary>
    [CreateAssetMenu(menuName = Constants.Menu + "Room")]
    public class Room : ScriptableObject
    {
        [SerializeField, Tooltip("The name of the room.")] string roomName = default;
        /// <summary> The name of the room. </summary>
        public string RoomName => roomName;

        [SerializeField, TextArea, Tooltip("The description shown to the player upon entering the room.")]
        string description = default;
        /// <summary> The description shown to the player upon entering the room. </summary>
        public string Description => description;

        [SerializeField, Tooltip("The exits connecting this room to other rooms.")]
        Exit[] exits = default;
        /// <summary> The exits connecting this room to other rooms. </summary>
        public Exit[] Exits => exits;

        [SerializeField, Tooltip("The interactable objects to be found in the room.")]
        InteractableObject[] objects = default;
        /// <summary> The interactable objects to be found in the room. </summary>
        public InteractableObject[] Objects => objects;
    }
}
