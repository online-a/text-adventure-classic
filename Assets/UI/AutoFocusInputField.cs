﻿using UnityEngine;
using InputField = TMPro.TMP_InputField;

namespace OnlineA.TextAdventure {
    [RequireComponent(typeof(InputField))]
    public class AutoFocusInputField : MonoBehaviour
    {
        void Start()
        {
            GetComponent<InputField>().text = string.Empty;
            SetFocus(true);
        }
        void OnEnable()
        {
            SetFocus(true);
        }

        void OnDisable()
        {
            SetFocus(false);
        }

        public void SetFocus(bool activate)
        {
            Debug.Log($"Selecting: {activate}");
            var inputField = GetComponent<InputField>();
            if (activate)
            {
                inputField.ActivateInputField();
                inputField.Select();
            }
            else
            {
                inputField.DeactivateInputField(true);
            }
        }
    }
}
