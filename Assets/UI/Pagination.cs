﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace OnlineA.TextAdventure
{
    public class Pagination : MonoBehaviour
    {
        [SerializeField] TMP_Text text = default;
        [Space]
        [SerializeField] Button previousPage = default;
        [SerializeField] Button nextPage = default;

        int MaxPage => text.GetTextInfo(text.text).pageCount;

        void Start()
        {
            previousPage.gameObject.SetActive(false);
            previousPage.gameObject.SetActive(false);
            OnTextChanged(text);
        }

        public void PageChange(int amount)
        {
            text.pageToDisplay = Mathf.Max(1, Mathf.Min(MaxPage, text.pageToDisplay + amount));
            EventSystem.current.SetSelectedGameObject(null); // deselect after clicking
            OnTextChanged(text);
        }

        public void OnTextChanged(TMP_Text t)
        {
            if (!t.isTextOverflowing)
            {
                previousPage.gameObject.SetActive(false);
                nextPage.gameObject.SetActive(false);
            }
            else
            {
                previousPage.gameObject.SetActive(t.pageToDisplay > 1);
                nextPage.gameObject.SetActive(t.pageToDisplay < MaxPage);
                nextPage.gameObject.SetActive(t.pageToDisplay < MaxPage);
            }
        }
    }

}